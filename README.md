#Simple string replacer to generate translations in Angular project
Replace all strings in angular project with ngx translate pipes!


**Be careful and commit everything before using this script!!! Regex sometimes targets some svg attributes, etc. :)**

#Usage
Go to angular project root folder and type `ngx-replace` in terminal. The script is looking for files in _./src/app_ location.
