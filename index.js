#!/usr/bin/env node
const path = require('path'), fs = require('fs');
const allFileLocations = [];
const _ = require('lodash');
const dot = require('dot-object');

/**
 * Find files recursively
 * @param startPath
 * @param filter
 */
function fromDir(startPath, filter) {
    if (!fs.existsSync(startPath)) {
        console.log("no dir ", startPath);
        return;
    }
    const files = fs.readdirSync(startPath);
    for (let i = 0; i < files.length; i++) {
        const filename = path.join(startPath, files[i]);
        const stat = fs.lstatSync(filename);
        if (stat.isDirectory()) {
            fromDir(filename, filter); //recurse
        } else if (filename.indexOf(filter) >= 0) {
            allFileLocations.push(filename);
        }
    }
}

fromDir('./src/app', '.html');

// Read all translations
const translations = {};
allFileLocations.forEach(fileLocation => {
    const file = fs.readFileSync(fileLocation).toString('utf8');
    translations[fileLocation] = file.replace(/((<style>)|(<style type=.+))((\s+)|(\S+)|(\r+)|(\n+))(.+)((\s+)|(\S+)|(\r+)|(\n+))(<\/style>)/g, '')
        .replace(/(<svg[\s\S]*<\/svg>)/g, '')
        .replace(/<[^>]+>/g, '')
        .replace(/\r?\n|\r/g, '')
        .replace('-->', '')
        .replace('-->', '')
        .replace('-->', '')
        .replace('-->', '')
        .trim()
        .split('  ')
        .filter(x => x).filter(x => {
            if (x.indexOf('{{') === -1 && x.indexOf('}}') === -1 && x.indexOf('"') === -1) {
                return x;
            }
        });
});

// Replace found translation strings and save translations to json
let row = {};
allFileLocations.forEach(fileLocation => {
    let file = fs.readFileSync(fileLocation).toString('utf8');
    translations[fileLocation].forEach(x => {
        file = file.replace(x, `{{'${_.camelCase(fileLocation.split('/').pop().split('.')[0])}.${_.camelCase(x)}' | translate}}`)
        row[`${_.camelCase(fileLocation.split('/').pop().split('.')[0])}.${_.camelCase(x)}`] = x;
    });
    fs.writeFileSync(fileLocation, file);
});

// Write .json file for translations
fs.writeFileSync('./translated-values.json', JSON.stringify(dot.object(row)));
console.log('Replacing completed.. translations are in ./translated-values.json file :)\n Be sure you check all files, regex can match some svg files and attributes.\n Feel free to create a pull request :)');
